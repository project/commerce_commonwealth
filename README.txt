DESCRIPTION
===========
Commonwealth bank, aka CBA (https://www.commbank.com.au/)
integration for the Drupal Commerce payment and checkout system.

Commonwealth bank of Australia use a system called Commweb for connecting
with their banking systems electronically.

3-party VPS allows access to their secure remote credit card gateway. There is
no need to install the Commweb payment client on the server.
There is no need for the extra security and validation that you'd need if you
were handing credit card detail on the site. The transactions are fully
validated and paid at the remote end, and the user is then redirected back to
the site.
